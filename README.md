# Mafia Online Development Tracker

Player / User Facing Repository to demonstrate the development progress of Mafia Online

You can play Mafia Online for free at [playmafia.online](https://playmafia.online)

Discussion and community guidance can be found on the [development forum](https://playmafia.online/forum/development)

# Versioning

Version numbers are based on the year and release number (for example 2019.04 is the 4th release of 2019)

# Source Code

The source code for Mafia Online is not currently available (it is not open source). Please do not ask for it. It may be released in due course, at my sole discretion.

# Contributors

[JohnoTheCoder](https://johnothecoder.uk) on behalf of [JTC Labs](https://jtclabs.coms)
